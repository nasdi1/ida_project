from django import forms

from .models import Photo


class ImageForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = ["url", "image_file"]


class ConvertorForm(forms.Form):
    height = forms.IntegerField(required=False)
    width = forms.IntegerField(required=False)
