from tempfile import NamedTemporaryFile
from urllib.request import urlopen

from django.core.files import File
from django.db import models


class Photo(models.Model):
    url = models.URLField(blank=True, null=True)
    image_file = models.ImageField(upload_to="", blank=True, null=True)
    converted_image = models.ImageField(upload_to="", blank=True, null=True)

    def save(self, *args, **kwargs):
        if self.url and not self.image_file:
            img_temp = NamedTemporaryFile(delete=True)
            img_temp.write(urlopen(self.url).read())
            img_temp.flush()
            self.image_file.save(f"{self.url.split('/')[-1]}", File(img_temp))
        if not self.converted_image:
            self.converted_image.save(
                f"converted_{self.image_file.name}", File(self.image_file)
            )
        super(Photo, self).save(*args, **kwargs)
