from PIL import Image

from convertor.models import Photo

from .forms import ConvertorForm, ImageForm


def check_image_form(form: ImageForm) -> bool:
    if form.is_valid():
        url = form.data.get("url")
        file = form.data.get("image_file")
        if file != "" and url != "":  # если не заполнены оба поля формы
            return False
        # если отправлен только файл
        if file != "":
            return True
        # если отправлен только url и имеет формат картники
        elif url != "" and any(
            [url.endswith(e) for e in [".jpg", ".jpeg", ".png", ".gif"]]
        ):
            return True
        else:
            return False
    else:
        return False


def check_convertor_form(form: ConvertorForm) -> bool:
    if form.is_valid():
        height = form.data.get("height")
        width = form.data.get("width")
        if height == "" and width == "":
            return False
        return True


def convert_image(pk: int, height: int, width: int):
    photo = Photo.objects.get(id=pk)

    height = photo.converted_image.height if height == "" else height
    width = photo.converted_image.width if width == "" else width

    image = Image.open(photo.converted_image.path)
    image = image.resize((int(width), int(height)), Image.LANCZOS)
    image.save(photo.converted_image.path, image.format)

    return photo
