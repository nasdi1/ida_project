from urllib.error import URLError

from django.contrib import messages
from django.shortcuts import redirect, render
from django.views import View

from .forms import ConvertorForm, ImageForm
from .models import Photo
from .service import check_convertor_form, check_image_form, convert_image


class PhotosView(View):
    def get(self, request):
        photos = Photo.objects.all()
        return render(request, "photos.html", {"photos": photos})


class PhotoView(View):
    def get(self, request):
        form = ImageForm()
        return render(request, "photo.html", {"form": form})

    def post(self, request):
        form = ImageForm(request.POST, request.FILES)
        if check_image_form(form):
            try:
                form.save()
                photo = form.instance
                return redirect(f"/photo/{photo.pk}")
            except URLError:
                messages.info(request, "incorrect  form")
                return render(request, "photo.html", {"form": form})

        messages.info(request, "incorrect  form")
        return render(request, "photo.html", {"form": form})


class ConvertorPhotoView(View):
    def get(self, request, pk):
        form = ConvertorForm()
        photo = Photo.objects.get(id=pk)
        return render(request, "convertor.html", {"form": form, "photo": photo})

    def post(self, request, pk):
        form = ConvertorForm(request.POST)
        if check_convertor_form(form):
            photo = convert_image(pk, form.data.get("height"), form.data.get("width"))

            form = ConvertorForm()
            return render(request, "convertor.html", {"form": form, "photo": photo})

        form = ConvertorForm()
        photo = Photo.objects.get(id=pk)
        return render(request, "convertor.html", {"form": form, "photo": photo})
