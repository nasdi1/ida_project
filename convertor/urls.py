from django.urls import path

from .views import ConvertorPhotoView, PhotosView, PhotoView

urlpatterns = [
    path("photo", PhotoView.as_view()),
    path("photo/<int:pk>", ConvertorPhotoView.as_view()),
    path("photos", PhotosView.as_view()),
]
