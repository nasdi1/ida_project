import tempfile
from io import BytesIO

import pytest
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.test import override_settings
from PIL import Image

from convertor.models import Photo


@pytest.mark.django_db
@override_settings(MEDIA_ROOT=tempfile.mkdtemp())
def test_url_saving():
    photo = Photo(
        url="https://w-dog.ru/wallpapers/10/17/361515952599980/priroda-pejzazh-gory-nebo-oblaka-sneg-zima-derevya.jpg"
    )
    photo.save()
    assert (
        photo.url
        == "https://w-dog.ru/wallpapers/10/17/361515952599980/priroda-pejzazh-gory-nebo-oblaka-sneg-zima-derevya.jpg"
    )
    assert (
        photo.image_file.name
        == "priroda-pejzazh-gory-nebo-oblaka-sneg-zima-derevya.jpg"
    )
    assert (
        photo.converted_image.name
        == "converted_priroda-pejzazh-gory-nebo-oblaka-sneg-zima-derevya.jpg"
    )


@pytest.mark.django_db
@override_settings(MEDIA_ROOT=tempfile.mkdtemp())
def test_file_saving():
    image = Image.new("RGBA", size=(50, 50), color=(256, 0, 0))
    image_file = BytesIO(image.tobytes())
    file = InMemoryUploadedFile(image_file, None, "test.jpg", "image/jpg", 1024, None)
    photo = Photo(image_file=file)
    photo.save()
    assert photo.image_file.name == "test.jpg"
    assert photo.converted_image.name == "converted_test.jpg"
    assert photo.url is None
