import tempfile

import pytest
from django.test import override_settings

from convertor.models import Photo


@pytest.mark.django_db
@override_settings(MEDIA_ROOT=tempfile.mkdtemp())
def test_add_photo_by_url(client):
    movies = Photo.objects.all()
    assert len(movies) == 0

    resp = client.post(
        "/photo",
        {
            "url": "https://w-dog.ru/wallpapers/10/17/361515952599980/priroda-pejzazh-gory-nebo-oblaka-sneg-zima"
            "-derevya.jpg",
            "image_file": "",
        },
    )

    assert resp.status_code == 302

    photos = Photo.objects.all()
    assert len(photos) == 1
