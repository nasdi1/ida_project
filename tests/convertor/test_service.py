import tempfile

import pytest
from django.test import override_settings
from django.test.client import RequestFactory

from convertor.forms import ConvertorForm
from convertor.models import Photo
from convertor.service import check_convertor_form, convert_image


@pytest.mark.parametrize(
    "height, width, result",
    [["", "", False], ["", "640", True], ["640", "", True], ["480", "480", True]],
)
def test_check_convertor_form(height, width, result):
    request = RequestFactory().post("/photo/1", data={"height": height, "width": width})
    form = ConvertorForm(request.POST)
    assert check_convertor_form(form) == result


@pytest.mark.django_db
@override_settings(MEDIA_ROOT=tempfile.mkdtemp())
def test_convert_image():
    photo = Photo(
        url="https://w-dog.ru/wallpapers/10/17/361515952599980/priroda-pejzazh-gory-nebo-oblaka-sneg-zima-derevya.jpg"
    )
    photo.save()

    assert photo.converted_image.width == 1920
    assert photo.converted_image.height == 1433

    photo = convert_image(photo.pk, height=640, width=320)

    assert photo.converted_image.height == 640
    assert photo.converted_image.width == 320
